package com.redigames.teslaminigame.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.redigames.teslaminigame.logic.TheCurrent;
import com.redigames.teslaminigame.physics.PhysUserData;
import com.redigames.teslaminigame.utils.Constants;

/**
 * Created by Radoslaw Grabowski on 7/10/2017.
 */

public class TeslaHero extends Actor
{
	private Texture teslaTexture;
	private Rectangle heroBounds;
	TheCurrent current;
	private Body rigidBody;

	public TeslaHero()
	{
		super();
		heroBounds = new Rectangle( 0, 0, 0, 0 );
	}

	public void Setup( Texture tex, Rectangle posSize, World physWorld, TheCurrent current )
	{
		this.heroBounds.set( posSize );
		heroBounds.setCenter( current.GetAbsoluteValueForScreenPos( posSize.y ), posSize.y );

		this.teslaTexture = tex;
		this.current = current;

		if( rigidBody == null )
		{
			BodyDef bodyDef = new BodyDef();
			bodyDef.type = BodyDef.BodyType.DynamicBody;

			Vector2 pos = new Vector2();
			heroBounds.getCenter( pos );

			bodyDef.position.set( pos.x, pos.y );
			PolygonShape shape = new PolygonShape();
			shape.setAsBox( posSize.width / 2, posSize.height / 2 );
			rigidBody = physWorld.createBody( bodyDef );
			rigidBody.createFixture( shape, 0.5f );
			rigidBody.resetMassData();

			PhysUserData ud = new PhysUserData();
			ud.collisionType = PhysUserData.CollisionType.COLLISION_TESLA;
			rigidBody.setUserData( ud );

			shape.dispose();
		}
		else
		{
			Vector2 pos = new Vector2();
			heroBounds.getCenter( pos );
			rigidBody.setTransform( pos.x, pos.y, 0 );
		}
	}

	@Override
	public void draw( Batch batch, float parentAlpha )
	{
		super.draw( batch, parentAlpha );

		if( Constants.DRAW_TEXTURES )
		{
			batch.draw( teslaTexture, heroBounds.x, heroBounds.y, heroBounds.width, heroBounds.height );
		}
	}

	@Override
	public void drawDebug( ShapeRenderer shapes )
	{
		super.drawDebug( shapes );

		shapes.setColor( 255, 0, 0, 0 );
		shapes.rect( heroBounds.x, heroBounds.y, heroBounds.width, heroBounds.height );
	}


	@Override
	public void act( float delta )
	{
		super.act( delta );

		// tesla follows current
		Vector2 pos = new Vector2();
		heroBounds.getCenter( pos );
		heroBounds.setCenter( current.GetAbsoluteValueForScreenPos( pos.y ), pos.y );

		rigidBody.setTransform( pos.x, pos.y, 0 );
	}

	public void dispose()
	{
		teslaTexture.dispose();
	}
}
