package com.redigames.teslaminigame.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.redigames.teslaminigame.utils.Constants;

/**
 * Created by Radoslaw Grabowski on 7/6/2017.
 */


public class Background extends Actor
{	private final TextureRegion textureRegion;
	private Rectangle region1;
	private Rectangle region2;
	private float speedMultiplier = 1.0f;

	public Background( Texture texture, float speedMultiplier )
	{
		setDebug( Constants.DEBUG_MODE );

		this.speedMultiplier = speedMultiplier;
		textureRegion = new TextureRegion( texture );
		region1 = new Rectangle( 0, Constants.GAME_HEIGHT, Constants.GAME_WIDTH, Constants.GAME_HEIGHT );
		region2 = new Rectangle( 0, 0, Constants.GAME_WIDTH, Constants.GAME_HEIGHT );
	}

	@Override
	public void act( float delta )
	{
		super.act( delta );

		region1.y -= delta * Constants.RUNNER_SPEED * speedMultiplier;
		region2.y -= delta * Constants.RUNNER_SPEED * speedMultiplier;

		if( region2.y < -Constants.GAME_HEIGHT )
		{
			region2.set( region1 );
			region1.set( 0, Constants.GAME_HEIGHT + region2.y, Constants.GAME_WIDTH, Constants.GAME_HEIGHT );
		}
	}

	@Override
	public void draw( Batch batch, float parentAlpha )
	{
		super.draw( batch, parentAlpha );

		if( Constants.DRAW_TEXTURES )
		{
			batch.draw( textureRegion, region1.x, region1.y, Constants.GAME_WIDTH, Constants.GAME_HEIGHT );
			batch.draw( textureRegion, region2.x, region2.y, Constants.GAME_WIDTH, Constants.GAME_HEIGHT );
		}
	}

	public void dispose()
	{
		textureRegion.getTexture().dispose();
	}
}