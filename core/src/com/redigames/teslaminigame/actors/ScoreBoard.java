package com.redigames.teslaminigame.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Radoslaw Grabowski on 8/3/2017.
 */

public class ScoreBoard extends Actor
{
	private Texture background;
	private int score;
	private Rectangle rect;

	public ScoreBoard()
	{
		super();
		score = 0;
	}

	public void setScore( int score )
	{
		this.score = score;
	}

	public void setup( Texture tex, Rectangle size )
	{
		background = tex;
		rect = size;
	}

	@Override
	public void act( float delta )
	{
		super.act( delta );
	}

	@Override
	public void draw( Batch batch, float parentAlpha )
	{
		super.draw( batch, parentAlpha );

		if( background == null )
			return;

		batch.draw( background, rect.x, rect.y, rect.width, rect.height );
	}

	@Override
	public void drawDebug( ShapeRenderer shapes )
	{
		super.drawDebug( shapes );
	}
}
