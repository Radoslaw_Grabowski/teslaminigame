package com.redigames.teslaminigame.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Pool;
import com.redigames.teslaminigame.physics.PhysUserData;
import com.redigames.teslaminigame.utils.Constants;

/**
 * Created by Radoslaw Grabowski on 7/6/2017.
 */

public class Obstacle extends Actor implements Pool.Poolable
{
	private Sprite sprite;
	private Body rigidBody;

	public Obstacle()
	{
		super();
	}

	public Rectangle getBounds()
	{
		if( sprite == null )
			return null;

		return new Rectangle( sprite.getX() + sprite.getWidth() / 2, sprite.getY() + sprite.getHeight() / 2, sprite.getWidth(), sprite.getHeight() );
	}

	public void Setup( Texture texture, boolean flipTexture, World physWorld, Rectangle rect )
	{
		sprite = new Sprite( texture );
		sprite.setSize( rect.width, rect.height );
		sprite.setPosition( rect.x, rect.y - Constants.GAME_HEIGHT );

		if( flipTexture )
			sprite.flip( true, false );

		if( rigidBody == null )
		{
			BodyDef bodyDef = new BodyDef();
			bodyDef.type = BodyDef.BodyType.StaticBody;

			Vector2 pos = new Vector2( sprite.getX() + sprite.getWidth() / 2, sprite.getY() + sprite.getHeight() / 2 );

			bodyDef.position.set( pos.x, pos.y );
			rigidBody = physWorld.createBody( bodyDef );

			PolygonShape shape = new PolygonShape();
			shape.setAsBox( rect.width, rect.height );
			rigidBody.createFixture( shape, 0.5f );
			rigidBody.resetMassData();
			shape.dispose();

			// Set user data
			PhysUserData ud = new PhysUserData();
			ud.collisionType = PhysUserData.CollisionType.COLLISION_OBSTACLE;
			rigidBody.setUserData( ud );
		}
		else
		{
			Vector2 pos = new Vector2();
			rigidBody.setTransform( pos.x, pos.y, 0 );

			PolygonShape shape = new PolygonShape();
			shape.setAsBox( rect.width, rect.height );
			rigidBody.createFixture( shape, 0.5f );

			shape.dispose();
		}
	}

	public void setPosition( Vector2 pos )
	{
		Vector2 center = new Vector2();
		rigidBody.setTransform( center.x, center.y, 0 );
		sprite.setPosition( pos.x, pos.y );

		//sprite.translate( 0, sprite.getY() - pos.y );
	}

	@Override
	public void reset()
	{
		super.remove();

		rigidBody.setLinearVelocity( 0, 0 );
		rigidBody.setAngularVelocity( 0 );
		rigidBody.destroyFixture( rigidBody.getFixtureList().first() );

		sprite = null;
	}

	@Override
	public void draw( Batch batch, float parentAlpha )
	{
		super.draw( batch, parentAlpha );

		if( Constants.DRAW_TEXTURES )
		{
			sprite.draw( batch );
			//batch.draw( sprite, rect.x, rect.y, rect.width, rect.height );
		}
	}

	@Override
	public void drawDebug( ShapeRenderer shapes )
	{
		super.drawDebug( shapes );
		shapes.setColor( 0, 0, 255, 0 );
		shapes.rect( sprite.getX(), sprite.getY(), sprite.getWidth(), sprite.getHeight() );
	}
}
