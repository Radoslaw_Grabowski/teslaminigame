package com.redigames.teslaminigame;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.redigames.teslaminigame.screens.BaseGameScreen;
import com.redigames.teslaminigame.screens.mainGame.StateGameScreen;
import com.redigames.teslaminigame.utils.Constants;

/**
 * Created by Radoslaw Grabowski on 05.07.2017.
 */

public class TeslaMinigame implements ApplicationListener
{
	private BaseGameScreen currentScreen;
	private AssetManager assetManager;
	private boolean assetLoadBegun = false;
	private boolean assetLoadEnded = false;

	@Override
	public void create()
	{
		if( Gdx.app.getType() == Application.ApplicationType.Android )
		{
			Constants.GAME_HEIGHT = Gdx.graphics.getHeight();
			Constants.GAME_WIDTH = Gdx.graphics.getWidth();

			Gdx.app.log( "TeslaMinigame", "Resolution changed to " + Constants.GAME_WIDTH + "x" + Constants.GAME_HEIGHT );
		}

		assetManager = new AssetManager();
		currentScreen = new StateGameScreen( assetManager );
		currentScreen.loadAssets();
	}

	@Override
	public void resize( int width, int height )
	{
		currentScreen.resize( width, height );
	}

	@Override
	public void render()
	{
		final float deltaTime = Gdx.graphics.getDeltaTime();

		// Do async asset loading
		while( !assetManager.update() )
		{
			// Call begin asset loading
			if( !assetLoadBegun )
			{
				assetLoadBegun = true;
				currentScreen.onAssetLoadingBegins();
			}

			// call rendering/update method while assets are loading
			currentScreen.renderOnAssetLoading( deltaTime );

			assetLoadEnded = true;
		}

		// Call after asset loading has finished
		if( assetLoadEnded )
		{
			assetLoadEnded = false;
			currentScreen.onAssetLoadingEnds();
		}

		// Call regular rendering
		currentScreen.render( deltaTime );
	}

	@Override
	public void pause()
	{
		currentScreen.pause();
	}

	@Override
	public void resume()
	{
		currentScreen.resume();
	}

	@Override
	public void dispose()
	{
		currentScreen.dispose();

		assetManager.dispose();
	}
}
