package com.redigames.teslaminigame.utils;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Radoslaw Grabowski on 05.07.2017.
 */

public class Constants
{
	public static final boolean PHYS_DEBUG = false;
	public static final boolean DEBUG_MODE = false;
	public static final boolean DRAW_TEXTURES = !DEBUG_MODE;

	// Screen proportions
	public static final float SCREEN_REFERENCE_WIDTH = 1440;
	public static final float SCREEN_REFERENCE_HEIGHT = 2560;
	private static final float DESKTOP_HEIGHT = 900;

	public static float GAME_WIDTH = ( SCREEN_REFERENCE_WIDTH * ( DESKTOP_HEIGHT / SCREEN_REFERENCE_HEIGHT ) );
	public static float GAME_HEIGHT = DESKTOP_HEIGHT;

	// Game settings
	public static final float RUNNER_SPEED = 150.0f;

	// Assets path
	public static final String BACKGROUND1_TEX_PATH = "minigame/background/background1.jpg";
	public static final String BACKGROUND2_TEX_PATH = "minigame/background/background2.png";
	public static final String BACKGROUND3_TEX_PATH = "minigame/background/background3.png";
	public static final String BACKGROUND4_TEX_PATH = "minigame/background/background4.png";

	public static final String TOUCH_BTN_TEX_PATH = "touchball.png";
	public static final String COG_TEX_PATH = "minigame/game-cog.png";
	public static final String GEM_TEX_PATH = "minigame/game-gem.png";
	public static final String OBSTACLE_TEX_PATH = "minigame/game-obstacle.png";
	public static final String GUI_SCORE_BOARD_TEX_PATH = "minigame/game-poins-board.png";
	public static final String TESLA_TEX_PATH = "minigame/game-tesla.png";


	// Sine current
	public static final int SINE_GRANULARITY = 200;

	public static final float SINE_CYCLES_PER_SCREEN = 1.5f;
	// Physics
	public static final Vector2 GRAVITY = new Vector2( 0f, -10f );

	// Grid
	public static final float GRID_Y_SIZE = 16*3;
	public static final float GRID_X_SIZE = 9*3;
	public static final float TEST_THRESHOLD_PERCENT = 0.3f;
	public static final Vector2 CELL_SIZE = new Vector2( GAME_WIDTH / GRID_X_SIZE, GAME_HEIGHT / GRID_Y_SIZE );

	// NPCs size
	public static final Vector2 ENEMY_SIZE = new Vector2( GAME_WIDTH / GRID_X_SIZE, GAME_HEIGHT / GRID_Y_SIZE );
	public static final Vector2 HERO_SIZE = new Vector2( GAME_WIDTH * 0.1f, GAME_WIDTH * 0.1f * 1.52f );

	// GUI size
	public static final Vector2 TOUCH_SIZE = new Vector2( GAME_WIDTH * 0.15f, GAME_WIDTH * 0.15f );

}