package com.redigames.teslaminigame.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Radoslaw Grabowski on 7/26/2017.
 */

public class TextureSizeManipulator
{
	// It assumes that texture was created in reference plain of size SCREEN_REFERENCE_HEIGHT x SCREEN_REFERENCE_WIDTH
	// In this case its 2560 x 1400
	// Function scales the texture without changing width-height ratio
	public static void FitSizeMaintainRatio( final Texture inTex, Vector2 outSize,
											 final boolean fitToHeight )
	{
		final float referenceScreenRatio = Constants.SCREEN_REFERENCE_HEIGHT / Constants.SCREEN_REFERENCE_WIDTH;
		final float currentScreenRatio = ( float ) Gdx.graphics.getHeight() / ( float ) Gdx.graphics.getWidth();

		if( fitToHeight || Math.abs( referenceScreenRatio - currentScreenRatio ) < 0.051f )
		{
			final float hhRatio = Gdx.graphics.getHeight() / Constants.SCREEN_REFERENCE_HEIGHT;
			outSize.set( ( float ) inTex.getWidth() * hhRatio, ( float ) inTex.getHeight() * hhRatio );
			return;
		}
		else
		{
			final float wwRatio = Gdx.graphics.getWidth() / Constants.SCREEN_REFERENCE_WIDTH;
			outSize.set( ( float ) inTex.getWidth() * wwRatio, ( float ) inTex.getHeight() * wwRatio );
			return;
		}
	}

	public static void fitToSizemaintainRatio( Vector2 inOutSize, final Vector2 inSizeToFit )
	{
		if( Math.abs( inOutSize.x - inSizeToFit.x ) >= Math.abs( inOutSize.y - inSizeToFit.y ) )
		{
			final float ratio = inOutSize.x / inSizeToFit.x;
			inOutSize.x *= ratio;
			inOutSize.y *= ratio;
		}
		else
		{
			final float ratio = inOutSize.y / inSizeToFit.y;
			inOutSize.x *= ratio;
			inOutSize.y *= ratio;
		}
	}

}
