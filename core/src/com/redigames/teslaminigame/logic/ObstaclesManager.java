package com.redigames.teslaminigame.logic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.redigames.teslaminigame.actors.Obstacle;
import com.redigames.teslaminigame.screens.mainGame.stages.GameStage;
import com.redigames.teslaminigame.utils.Constants;
import com.redigames.teslaminigame.utils.TextureSizeManipulator;

import java.util.Random;

/**
 * Created by Radoslaw Grabowski on 7/6/2017.
 */

public class ObstaclesManager
{
	private Array<Obstacle> activeObstacles = new Array<Obstacle>();
	private Pool<Obstacle> obstaclesPool = new Pool<Obstacle>()
	{
		@Override
		protected Obstacle newObject()
		{
			return new Obstacle();
		}
	};
	private World physWorld;
	private GameStage gameStage;

	private Texture cogTex;
	private Texture gemTex;
	private Texture obstacleTex;
	private Random rand;
	private Array<ObstacleDef> obstaclesToAdd;

	private class ObstacleDef
	{
		public Texture texture;
		public float xSizePx;
		public float ySizePX;
		public int xSizeGrid;
		public int ySizeGrid;

		TheGrid.SearchPosition preferedPos;
	}

	public ObstaclesManager( GameStage stage, World physWorld )
	{
		this.physWorld = physWorld;
		this.gameStage = stage;
		this.rand = new Random();
		this.obstaclesToAdd = new Array<ObstacleDef>();
	}

	public void onAssetLoadingEnds( AssetManager assetManager )
	{
		cogTex = assetManager.get( Constants.COG_TEX_PATH );
		gemTex = assetManager.get( Constants.GEM_TEX_PATH );
		obstacleTex = assetManager.get( Constants.OBSTACLE_TEX_PATH );
	}

	public boolean areObstaclesOverlaping()
	{
		for( int i = 0; i < activeObstacles.size - 1; ++i )
		{
			Obstacle first = activeObstacles.get( i );

			// Resize obstacle rectangle to avoid edges overlap
			Rectangle rect1 = first.getBounds();
			rect1.width *= 0.95f;
			rect1.height *= 0.95f;

			rect1.x += 0.5f * ( first.getBounds().width - rect1.width );
			rect1.y += 0.5f * ( first.getBounds().height - rect1.height );

			for( int j = i + 1; j < activeObstacles.size; ++j )
			{
				Obstacle second = activeObstacles.get( j );

				if( rect1.overlaps( second.getBounds() ) )
					return true;
			}
		}

		return false;
	}

	private void generateCollectables()
	{
		final int numPickables = 1 + rand.nextInt( 5 );

		// Pickups
		Vector2 targetSize = new Vector2();
		TextureSizeManipulator.FitSizeMaintainRatio( gemTex, targetSize, true );

		int sizeTexGridX = ( int ) ( targetSize.x / Constants.CELL_SIZE.x + 0.5f );
		int sizeTexGridY = ( int ) ( targetSize.y / Constants.CELL_SIZE.y + 0.5f );

		TextureSizeManipulator.fitToSizemaintainRatio( targetSize, new Vector2( sizeTexGridX * Constants.CELL_SIZE.x, sizeTexGridY * Constants.CELL_SIZE.y ) );

		for( int i = 0; i < numPickables; ++i )
		{
			ObstacleDef def = new ObstacleDef();
			def.texture = gemTex;
			def.xSizePx = targetSize.x;
			def.ySizePX = targetSize.y;
			def.xSizeGrid = sizeTexGridX;
			def.ySizeGrid = sizeTexGridY;
			def.preferedPos = TheGrid.SearchPosition.SP_ANY;

			obstaclesToAdd.add( def );
		}
	}

	private void generateObstaclesToAdd()
	{
		final int numBigObstaclesLeft = 1 + rand.nextInt( 3 );
		final int numBigObstaclesRight = 1 + rand.nextInt( 3 );
		final int numObstacles = 3 + rand.nextInt( 12 );

		// Big obstacles left
		Vector2 targetSize = new Vector2();
		TextureSizeManipulator.FitSizeMaintainRatio( obstacleTex, targetSize, true );

		int sizeTexGridX = ( int ) ( targetSize.x / Constants.CELL_SIZE.x + 0.5f );
		int sizeTexGridY = ( int ) ( targetSize.y / Constants.CELL_SIZE.y + 0.5f );

		TextureSizeManipulator.fitToSizemaintainRatio( targetSize, new Vector2( sizeTexGridX * Constants.CELL_SIZE.x, sizeTexGridY * Constants.CELL_SIZE.y ) );

		for( int i = 0; i < numBigObstaclesLeft; ++i )
		{
			ObstacleDef def = new ObstacleDef();
			def.texture = obstacleTex;
			def.xSizePx = targetSize.x;
			def.ySizePX = targetSize.y;
			def.xSizeGrid = sizeTexGridX;
			def.ySizeGrid = sizeTexGridY;
			def.preferedPos = TheGrid.SearchPosition.SP_LEFT_SIDE;

			obstaclesToAdd.add( def );
		}

		// Big obstacles right
		for( int i = 0; i < numBigObstaclesRight; ++i )
		{
			ObstacleDef def = new ObstacleDef();
			def.texture = obstacleTex;
			def.xSizePx = targetSize.x;
			def.ySizePX = targetSize.y;
			def.xSizeGrid = sizeTexGridX;
			def.ySizeGrid = sizeTexGridY;
			def.preferedPos = TheGrid.SearchPosition.SP_RIGHT_SIDE;

			obstaclesToAdd.add( def );
		}

		// Regular obstacles
		TextureSizeManipulator.FitSizeMaintainRatio( cogTex, targetSize, true );

		sizeTexGridX = ( int ) ( targetSize.x / Constants.CELL_SIZE.x + 0.5f );
		sizeTexGridY = ( int ) ( targetSize.y / Constants.CELL_SIZE.y + 0.5f );

		TextureSizeManipulator.fitToSizemaintainRatio( targetSize, new Vector2( sizeTexGridX * Constants.CELL_SIZE.x, sizeTexGridY * Constants.CELL_SIZE.y ) );

		for( int i = 0; i < numObstacles; ++i )
		{
			ObstacleDef def = new ObstacleDef();
			def.texture = cogTex;
			def.xSizePx = targetSize.x;
			def.ySizePX = targetSize.y;
			def.xSizeGrid = sizeTexGridX;
			def.ySizeGrid = sizeTexGridY;
			def.preferedPos = TheGrid.SearchPosition.SP_ANY;

			obstaclesToAdd.add( def );
		}
	}

	public void addCollectables( TheGrid grid )
	{
		generateCollectables();

		for( int i = 0; i < obstaclesToAdd.size; ++i )
		{
			ObstacleDef def = obstaclesToAdd.get( i );

			// rand search position
			final int gridX = 1 + rand.nextInt( ( int ) Constants.GRID_X_SIZE );
			final int gridY = 1 + rand.nextInt( ( int ) Constants.GRID_Y_SIZE );

			TheCell cell = grid.findCellSpace( gridX, gridY, def.xSizeGrid, def.ySizeGrid, def.preferedPos );

			if( cell != null )
			{
				//Gdx.app.log( "ObstacleManager", "Found cell size: " + ySize + "x" + xSize + " rect px: " + cell.getBounds.toString() );

				Obstacle obstacle = obstaclesPool.obtain();
				obstacle.Setup( def.texture, false, physWorld, cell.bounds );
				activeObstacles.add( obstacle );

				grid.bindCellWithObstacle( cell, obstacle );

				gameStage.addActor( obstacle );
			}
		}

	}

	public void addObstacles( TheGrid grid )
	{
		rand.setSeed( rand.nextLong() );

		generateObstaclesToAdd();

		for( int i = 0; i < obstaclesToAdd.size; ++i )
		{
			ObstacleDef def = obstaclesToAdd.get( i );

			TheCell cell;

			if( def.texture == obstacleTex )
			{
				cell = grid.findRandomCellSpace( def.xSizeGrid, def.ySizeGrid, def.preferedPos );
			}
			else
			{
				// rand search position
				final int gridX = 1 + rand.nextInt( ( int ) Constants.GRID_X_SIZE );
				final int gridY = 1 + rand.nextInt( ( int ) Constants.GRID_Y_SIZE );

				cell = grid.findCellSpace( gridX, gridY, def.xSizeGrid, def.ySizeGrid, def.preferedPos );
			}

			if( cell != null )
			{
				//Gdx.app.log( "ObstacleManager", "Found cell size: " + ySize + "x" + xSize + " rect px: " + cell.getBounds.toString() );

				Obstacle obstacle = obstaclesPool.obtain();
				boolean flip = def.texture == obstacleTex && def.preferedPos == TheGrid.SearchPosition.SP_RIGHT_SIDE;
				obstacle.Setup( def.texture, flip, physWorld, cell.bounds );
				activeObstacles.add( obstacle );

				grid.bindCellWithObstacle( cell, obstacle );

				gameStage.addActor( obstacle );
			}
		}

		// Remove obstacle definitions
		obstaclesToAdd.clear();

		if( Constants.DEBUG_MODE && areObstaclesOverlaping() )
		{
			Gdx.app.log( "DEBUG", "Obstacles overlap" );
		}

	}

	public void removeObstacle( Obstacle obstacle )
	{
		activeObstacles.removeValue( obstacle, true );
		obstaclesPool.free( obstacle );
	}


	public void update()
	{
	}

	public void dispose()
	{
		activeObstacles.clear();
		obstaclesPool.clear();
	}
}
