package com.redigames.teslaminigame.logic;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import com.redigames.teslaminigame.actors.Obstacle;
import com.redigames.teslaminigame.utils.Constants;

/**
 * Created by Radoslaw Grabowski on 7/19/2017.
 */

public class TheCell implements Pool.Poolable
{
	public int gridXpos;
	public int gridYpos;

	public  Rectangle bounds;
	public Obstacle obstacle;

	public TheCell()
	{
		bounds = new Rectangle( 0, 0, 1, 1 );
		obstacle = null;

		gridXpos = -1;
		gridYpos = -1;
	}

	@Override
	public void reset()
	{
		bounds.set( 0, 0, 1, 1 );
		obstacle = null;

		gridXpos = -1;
		gridYpos = -1;
	}

	public void bindWithObstacle( Obstacle obstacle )
	{
		this.obstacle = obstacle;
	}

	public void update( float deltaTime )
	{
		// Move the cell downward
		bounds.y -= Constants.RUNNER_SPEED * deltaTime;

		if( obstacle != null )
		{
			//TODO: obstacle movement with the cell
			obstacle.setPosition( new Vector2( bounds.x, bounds.y ) );
		}
	}
}
