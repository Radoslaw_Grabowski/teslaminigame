package com.redigames.teslaminigame.logic;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.redigames.teslaminigame.utils.Constants;

/**
 * Created by Radoslaw Grabowski on 7/7/2017.
 */

public class TheCurrent extends Actor
{
	private float sineAmplitude = 0f;
	private float sineOffsetRad = 0f;
	//private float sineMovementSpeed = 0f;
	private float sineRadToPixRatio = 1f;

	public void SetAmplitude( float amplitude )
	{
		sineAmplitude = amplitude;
	}

	public TheCurrent()
	{
		setDebug( Constants.DEBUG_MODE );
		resize( ( int ) Constants.GAME_WIDTH, ( int ) Constants.GAME_HEIGHT );
	}

	public float GetAbsoluteValueForScreenPos( float scrY )
	{
		float val = Constants.GAME_WIDTH / 2f + sineAmplitude * GetSineXValueForScreenYPos( scrY );
		return val;
	}

	private float GetAbsoluteValue( float scrY, float amplitudeSign )
	{
		final float hScrWidth = Constants.GAME_WIDTH / 2f;
		final float val = hScrWidth + hScrWidth * amplitudeSign * GetSineXValueForScreenYPos( scrY );

		return val;
	}

	public float GetSineXValueForScreenYPos( float scrYPos )
	{
		float cyclePixelHeight = Constants.GAME_HEIGHT / Constants.SINE_CYCLES_PER_SCREEN;
		float val = scrYPos % cyclePixelHeight; // get rid of whole cycles
		val /= cyclePixelHeight;    // this cycle percentage
		val *= Math.PI * 2f;    // convert to radians

		val += sineOffsetRad;

		val = ( float ) Math.sin( val );    // get sine val
		return val;
	}

	public void resize( int width, int height )
	{
		float cyclePixelHeight = height / Constants.SINE_CYCLES_PER_SCREEN;
		float cycleRadianHeght = 2f * ( float ) Math.PI;
		sineRadToPixRatio = cycleRadianHeght / cyclePixelHeight;
		//sineMovementSpeed = 0.7f * sineRadToPixRatio;
	}

	@Override
	public void draw( Batch batch, float parentAlpha )
	{
		super.draw( batch, parentAlpha );
	}

	@Override
	public void act( float delta )
	{
		super.act( delta );

		sineOffsetRad += ( sineRadToPixRatio /*+ sineMovementSpeed*/ ) * delta * Constants.RUNNER_SPEED;
		if( sineOffsetRad > ( float ) Math.PI * 2f )
		{
			sineOffsetRad -= ( float ) Math.PI * 2f;
		}
	}

	@Override
	public void drawDebug( ShapeRenderer shapes )
	{
		super.drawDebug( shapes );

		///////////////////////////////////
		// Draw alternating current line //
		///////////////////////////////////
		Vector2 beginPoint = new Vector2( Constants.GAME_WIDTH / 2f + sineAmplitude * GetSineXValueForScreenYPos( 0 ), 0 );
		Vector2 endPoint = new Vector2( 0, 0 );

		final float drawStep = Constants.GAME_HEIGHT / Constants.SINE_GRANULARITY;

		shapes.setColor( 255, 255, 0, 128 );
		for( float scrY = drawStep; scrY - drawStep < Constants.GAME_HEIGHT; scrY += drawStep )
		{
			endPoint.set( GetAbsoluteValueForScreenPos( scrY ), scrY );
			shapes.line( beginPoint, endPoint );
			beginPoint.set( endPoint );
		}

		/////////////////////////////////////////////
		// Draw alternating current zero crossings //
		/////////////////////////////////////////////
		final float cyclePixelHeight = Constants.GAME_HEIGHT / Constants.SINE_CYCLES_PER_SCREEN;
		float sineOffsetPixel = ( sineOffsetRad / ( 2f * ( float ) Math.PI ) ) * cyclePixelHeight;
		sineOffsetPixel = sineOffsetPixel % ( cyclePixelHeight / 2 );

		shapes.setColor( 255, 0, 0, 0 );
		for( float y = Constants.GAME_HEIGHT - sineOffsetPixel; y > 0; y -= cyclePixelHeight / 2 )
		{
			beginPoint.set( 0, y );
			endPoint.set( Constants.GAME_WIDTH, y );

			shapes.line( beginPoint, endPoint );
		}
	}

	public void dispose()
	{
	}
}
