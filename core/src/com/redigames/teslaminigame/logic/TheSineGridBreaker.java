package com.redigames.teslaminigame.logic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.redigames.teslaminigame.utils.Constants;

import java.util.Random;

/**
 * Created by Radoslaw Grabowski on 7/25/2017.
 */

public class TheSineGridBreaker
{
	private final float minTimeout = 0.8f;
	private final float maxTimeout = 3f;


	private Random rand;
	private float maxAmplitude;
	private float maxMoveSpeed;

	private float currAmplitude;
	private float currSpeed;
	private float currTimeut;

	public TheSineGridBreaker( float maxAmplitude )
	{
		this.maxAmplitude = maxAmplitude;
		rand = new Random();
		maxMoveSpeed = ( Constants.GAME_WIDTH / maxTimeout ) * 1.5f;

		currTimeut = getRandomFloat( minTimeout, maxTimeout );

		currAmplitude = getRandomFloat( 0, this.maxAmplitude );
		currAmplitude *= Math.signum( rand.nextFloat() - rand.nextFloat() );

		currSpeed = getRandomFloat( this.maxMoveSpeed * 0.1f, this.maxMoveSpeed );
		currSpeed *= Math.signum( rand.nextFloat() - rand.nextFloat() );
	}

	public float getCurrAmplitude()
	{
		return currAmplitude;
	}

	public void setMaxAmplitude( float maxAmplitude )
	{
		this.maxAmplitude = maxAmplitude;
		currAmplitude = getRandomFloat( 0, this.maxAmplitude );
	}

	public void update( float deltaTime )
	{
		currAmplitude += currSpeed * deltaTime;

		currAmplitude = MathUtils.clamp( currAmplitude, -maxAmplitude, maxAmplitude );
		currTimeut -= deltaTime;


		// change speed and aplitude
		if( currTimeut < 0 )
		{
			currTimeut = getRandomFloat( minTimeout, maxTimeout );

			currSpeed = getRandomFloat( maxMoveSpeed * 0.1f, maxMoveSpeed );
			currSpeed *= Math.signum( rand.nextFloat() - rand.nextFloat() );
		}
	}

	private float getRandomFloat( float min, float max )
	{
		if( rand == null )
			return 0.0f;

		float value = min + rand.nextFloat() * ( max - min );
		return value;
	}
}
