package com.redigames.teslaminigame.logic;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.redigames.teslaminigame.actors.Obstacle;
import com.redigames.teslaminigame.utils.Constants;

import java.util.Arrays;

/**
 * Created by Radoslaw Grabowski on 7/19/2017.
 */

public class TheGrid extends Actor
{
	private TheCurrent current;
	private ObstaclesManager obstaclesManager;

	private Vector2 cellSize;
	private Vector2 gridSize;
	private int numVoidRows;
	private Rectangle worldBounds;

	private Array<TheSineGridBreaker> gridBreakers = new Array<TheSineGridBreaker>();

	private TheCell[][] freeCells;
	private Array<TheCell> activeCells = new Array<TheCell>();
	private Pool<TheCell> cellPool = new Pool<TheCell>()
	{
		@Override
		protected TheCell newObject()
		{
			return new TheCell();
		}
	};

	public TheGrid( TheCurrent current, ObstaclesManager obstaclesManager, float gameWidth,
					float gameHeight )
	{
		setDebug( Constants.DEBUG_MODE );

		this.current = current;
		this.obstaclesManager = obstaclesManager;

		this.worldBounds = new Rectangle( 0, 0, gameWidth, gameHeight );
		this.gridSize = new Vector2( Constants.GRID_X_SIZE, Constants.GRID_Y_SIZE );
		this.cellSize = new Vector2( worldBounds.width / gridSize.x, worldBounds.height / gridSize.y );
		this.numVoidRows = ( int ) ( gridSize.y * Constants.SINE_CYCLES_PER_SCREEN * 0.5f ) + 1;
		this.freeCells = new TheCell[( int ) gridSize.x][numVoidRows];

		// Setup grid breakers to limit amount of cells
		TheSineGridBreaker breaker = new TheSineGridBreaker( worldBounds.width * 0.5f - Constants.TOUCH_SIZE.x * 0.5f );
		gridBreakers.add( breaker );

//		breaker = new TheSineGridBreaker( worldBounds.x * 0.5f - Constants.TOUCH_SIZE.x * 0.5f );
//		gridBreakers.add( breaker );
	}

	public void resize( float newWidth, float newHeight )
	{
		this.worldBounds = new Rectangle( 0, 0, newWidth, newHeight );
		this.cellSize = new Vector2( worldBounds.width / gridSize.x, worldBounds.height / gridSize.y );

		for( TheSineGridBreaker br : gridBreakers )
		{
			br.setMaxAmplitude( worldBounds.width * 0.5f - Constants.TOUCH_SIZE.x * 0.5f );
		}
	}

	public void resizeGrid( float newColsNum, float newRowsNum )
	{
		gridSize.set( newColsNum, newRowsNum );
		cellSize = new Vector2( worldBounds.width / gridSize.x, worldBounds.height / gridSize.y );
		numVoidRows = ( int ) ( gridSize.y * Constants.SINE_CYCLES_PER_SCREEN * 0.5f ) + 1;

		freeCells = new TheCell[( int ) gridSize.x][numVoidRows];
	}

	private void fillVoidWithCells( float startYPos )
	{
		for( int y = 0; y < numVoidRows; ++y )
		{
			for( int x = 0; x < gridSize.x; ++x )
			{
				TheCell spawnCell = cellPool.obtain();
				spawnCell.bounds.set(
						x * cellSize.x,
						y * cellSize.y + startYPos,
						cellSize.x,
						cellSize.y
				);

				spawnCell.gridXpos = x;
				spawnCell.gridYpos = y;

				freeCells[x][y] = null;
				freeCells[x][y] = spawnCell;
			}
		}
	}

	private void removeOverlappingCells()
	{
		final float halfScrWidth = worldBounds.width * 0.5f;
		Rectangle heroArea = new Rectangle( 0, 0, Constants.HERO_SIZE.x * ( 1.0f + Constants.TEST_THRESHOLD_PERCENT ), Constants.HERO_SIZE.y * ( 1.0f + Constants.TEST_THRESHOLD_PERCENT ) );

		TheCell startCell = freeCells[0][0];

		// The amount of cell rows that cover while height of hero
		final int cellRowsCoverHero = ( int ) Math.ceil( ( double ) ( heroArea.height * ( 1.0f + Constants.TEST_THRESHOLD_PERCENT ) ) / ( double ) cellSize.y );

		// since its simulation, simulate cell movement downwards
		final float breakerDeltaTime = Constants.CELL_SIZE.y / Constants.RUNNER_SPEED;

		for( int heroTestRow = -cellRowsCoverHero; heroTestRow < numVoidRows + cellRowsCoverHero; ++heroTestRow )
		{
			float targetYPos = startCell.bounds.y + ( startCell.bounds.height / 2 ) + ( startCell.bounds.height * ( float ) heroTestRow );
			float sineValue = current.GetSineXValueForScreenYPos( targetYPos );

			for( TheSineGridBreaker br : gridBreakers )
			{
				br.update( breakerDeltaTime );
				float targetXPos = sineValue * br.getCurrAmplitude() + halfScrWidth;

				heroArea.setCenter( targetXPos, targetYPos );

				// TODO: optimize amount of test by changing amount of cell tested per hero area step
				for( int y = 0; y < numVoidRows; ++y )
				{
					for( int x = 0; x < gridSize.x; ++x )
					{
						TheCell testCell = freeCells[x][y];

						if( testCell == null )
							continue;

						if( testCell.obstacle != null )
							continue;

						if( heroArea.overlaps( testCell.bounds ) )
						{
							freeCells[x][y] = null;
							cellPool.free( testCell );
						}
					}
				}
			}
		}
	}

	private void addCollectables()
	{
		obstaclesManager.addCollectables( this );
	}

	private void addObstacles()
	{
		// Process adding obstacles
		obstaclesManager.addObstacles( this );
	}

	public void removeObstacle( Obstacle obstacle )
	{
		obstaclesManager.removeObstacle( obstacle );
	}

	public void update( float deltaTime )
	{
		// Fill void above visible screen
		boolean generated = false;
		if( activeCells.size == 0 )    // When we just started and there's no cells on the screen
		{
			fillVoidWithCells( worldBounds.height );
			addCollectables();
			removeOverlappingCells();
			addObstacles();

			generated = true;
		}
		else
		{
			TheCell lastCell = activeCells.get( activeCells.size - 1 );
			if( lastCell != null && lastCell.bounds.y <= worldBounds.height )    // Last generated cell's yPos is below top screen line, generate new cells
			{
				fillVoidWithCells( lastCell.bounds.y + cellSize.y );
				addCollectables();
				removeOverlappingCells();
				addObstacles();

				generated = true;
			}
		}

		if( generated )
		{
			// Add new active cells and clear active cells array
			for( int y = 0; y < numVoidRows; ++y )
			{
				for( int x = 0; x < gridSize.x; ++x )
				{
					if( freeCells[x][y] != null )
					{
						activeCells.add( freeCells[x][y] );
						freeCells[x][y] = null;
					}
				}
			}
		}

		// Free cells that are outside of world bounds
		for( int i = 0; i < activeCells.size; ++i )
		{
			TheCell cell = activeCells.get( i );
			cell.update( deltaTime );

			if( cell.bounds.y + cell.bounds.height < 0 )
			{
				activeCells.removeIndex( i );

				if( cell.obstacle != null )
					removeObstacle( cell.obstacle );

				cellPool.free( cell );
				--i;
			}
		}
	}

	enum SearchPosition
	{
		SP_ANY,
		SP_LEFT_SIDE,
		SP_RIGHT_SIDE,
	}

	public TheCell findCellSpace( int xGridStart, int yGridStart, int xSize, int ySize,
								  SearchPosition sp )
	{
		TheCell[][] obstacleMask = new TheCell[xSize][ySize];

		// Clamp search position
		int startX = Math.max( 0, Math.min( ( int ) gridSize.x - xSize, xGridStart ) );
		int endX = ( int ) gridSize.x - xSize;

		// Search position either on left or right side
		if( sp == SearchPosition.SP_LEFT_SIDE )
		{
			startX = 0;
			endX = 1;
		}
		else if( sp == SearchPosition.SP_RIGHT_SIDE )
		{
			startX = endX;
		}

		final int endY = numVoidRows - ySize;
		final int startY = Math.max( 0, Math.min( numVoidRows - ySize, yGridStart ) );

		// Scan every position on the grid
		// TODO: optimize empty space search
		for( int x = startX; x <= endX; ++x )
		{
			for( int y = startY; y <= endY; ++y )
			{
				// Check if obstacle placed on the grid have empty cells under
				boolean doBreak = false;

				for( int yC = 0; yC < ySize; ++yC )
				{
					if( doBreak )
						break;

					for( int xC = 0; xC < xSize; ++xC )
					{
						TheCell emptyCell = freeCells[x + xC][y + yC];
						if( emptyCell == null )
						{
							doBreak = true;

							// Clean the mask
							for( TheCell[] col : obstacleMask )
							{
								Arrays.fill( col, null );
							}

							break;
						}
						else if( emptyCell.obstacle != null )
						{
							doBreak = true;

							// Clean the mask
							for( TheCell[] col : obstacleMask )
							{
								Arrays.fill( col, null );
							}

							break;
						}

						obstacleMask[xC][yC] = emptyCell;
					}
				}

				if( doBreak )
					continue;

				// Found space, combine cells and return
				TheCell resultCell = obstacleMask[0][0];

				// Remove from free cells pool
				freeCells[resultCell.gridXpos][resultCell.gridYpos] = null;

				for( int yPos = 0; yPos < ySize; ++yPos )
				{
					for( int xPos = 0; xPos < xSize; ++xPos )
					{
						if( xPos == 0 && yPos == 0 )
							continue;

						// Merge all spaces
						TheCell cellMerge = obstacleMask[xPos][yPos];

						// Remove combined cells
						freeCells[cellMerge.gridXpos][cellMerge.gridYpos] = null;
						resultCell.bounds.merge( cellMerge.bounds );

						// Free from pool
						cellPool.free( cellMerge );
					}
				}

				return resultCell;
			}
		}

		// No cell found
		return null;
	}

	public TheCell findRandomCellSpace( int xSize, int ySize, SearchPosition sp )
	{
		return findCellSpace( 0, 0, xSize, ySize, sp );
	}

	public void bindCellWithObstacle( TheCell cell, Obstacle obstacle )
	{
		activeCells.add( cell );
		cell.bindWithObstacle( obstacle );
	}

//	private boolean IsMergedCellOverlapping( TheCell cell )
//	{
//		// Make cell smaller so we exclude edges ovelap
//		Rectangle rect = new Rectangle();
//		rect.set( cell.bounds );
//		rect.width *= 0.95f;
//		rect.height *= 0.95f;
//
//		rect.x += 0.5f * ( cell.bounds.width - rect.width );
//		rect.y += 0.5f * ( cell.bounds.height - rect.height );
//
//
//		for( int y = 0; y < numVoidRows; ++y )
//		{
//			for( int x = 0; x < gridSize.x; ++x )
//			{
//				if( cell == freeCells[x][y] )
//					continue;
//
//				TheCell other = freeCells[x][y];
//				if( other == null )
//					continue;
//
//				if( rect.overlaps( other.bounds ) )
//					return true;
//			}
//		}
//
//		return false;
//	}

//	private boolean IsCellOverlapping( TheCell[][] cellArray, int xSize, int ySize )
//	{
//		TheCell freeFirst = cellArray[0][0];
//		TheCell freeLast = cellArray[xSize - 1][ySize - 1];
//
//		for( int y = 0; y < ySize; ++y )
//		{
//			for( int x = 0; x < xSize; ++x )
//			{
//				TheCell freeTest = cellArray[x][y];
//
//				for( int yFree = 0; yFree < numVoidRows; ++yFree )
//				{
//					for( int xFree = 0; xFree < gridSize.x; ++xFree )
//					{
//						// exclude found cells
//						if( yFree >= freeFirst.gridYpos &&
//							yFree <= freeLast.gridYpos &&
//							xFree >= freeFirst.gridXpos &&
//							xFree <= freeLast.gridXpos
//								)
//							continue;
//
//						TheCell other = freeCells[xFree][yFree];
//						if( other == null )
//							continue;
//
//						// Make cell smaller so we exclude edges ovelap
//						Rectangle rect = new Rectangle();
//						rect.set( other.bounds );
//						rect.width *= 0.95f;
//						rect.height *= 0.95f;
//
//						rect.x += 0.5f * ( freeTest.bounds.width - rect.width );
//						rect.y += 0.5f * ( freeTest.bounds.height - rect.height );
//
//						if( freeTest.bounds.overlaps( rect ) )
//							return true;
//					}
//
//				}
//			}
//		}
//
//		return false;
//	}

//	public boolean areCellsOfCorretSize()
//	{
//		for( int yFree = 0; yFree < numVoidRows; ++yFree )
//		{
//			for( int xFree = 0; xFree < gridSize.x; ++xFree )
//			{
//				TheCell other = freeCells[xFree][yFree];
//				if( other == null )
//					continue;
//
//				if( Math.abs( other.bounds.width - cellSize.x ) > 0.5f || Math.abs( other.bounds.height - cellSize.y ) > 0.5f )
//					return false;
//			}
//		}
//
//		return true;
//	}

	@Override
	public void draw( Batch batch, float parentAlpha )
	{
		super.draw( batch, parentAlpha );
	}

	@Override
	public void drawDebug( ShapeRenderer shapes )
	{
		super.drawDebug( shapes );

		if( !Constants.DEBUG_MODE )
			return;

		TheCell cell;
		for( int i = 0; i < activeCells.size; ++i )
		{
			cell = activeCells.get( i );

			if( cell.obstacle != null ) // draw obstacle
			{
				shapes.setColor( 0, 0, 50, 0 );

				// mark cross
				shapes.line( cell.bounds.x, cell.bounds.y, cell.bounds.x + cell.bounds.width, cell.bounds.y + cell.bounds.height );
				shapes.line( cell.bounds.x, cell.bounds.y + cell.bounds.height, cell.bounds.x + cell.bounds.width, cell.bounds.y );
			}
			else    // draw empty cell
			{
				shapes.setColor( 0, 50, 0, 0 );
				shapes.rect( cell.bounds.x, cell.bounds.y, cell.bounds.width, cell.bounds.height );

				// elipse of empty cell size
				shapes.ellipse( cell.bounds.x, cell.bounds.y, cell.bounds.width, cell.bounds.height );
			}

		}
	}
}
