package com.redigames.teslaminigame.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;

/**
 * Created by Radoslaw Grabowski on 7/26/2017.
 */

public abstract class BaseGameScreen implements Screen
{
	protected AssetManager assetManager;

	public BaseGameScreen( AssetManager assetManager )
	{
		this.assetManager = assetManager;
	}

	public abstract void loadAssets();

	public abstract void onAssetLoadingBegins();

	public abstract void renderOnAssetLoading( float deltaTime );

	public abstract void onAssetLoadingEnds();
}
