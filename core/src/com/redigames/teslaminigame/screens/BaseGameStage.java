package com.redigames.teslaminigame.screens;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by Radoslaw Grabowski on 7/26/2017.
 */

public abstract class BaseGameStage extends Stage
{
	protected AssetManager assetManager;

	public BaseGameStage( AssetManager assetManager )
	{
		this.assetManager = assetManager;
	}

	public abstract void loadAssets();

	public abstract void resize( int width, int height );

	public abstract void show();

	public abstract void onAssetLoadingBegins();

	public abstract void renderOnAssetLoading( float deltaTime );

	public abstract void onAssetLoadingEnds();
}
