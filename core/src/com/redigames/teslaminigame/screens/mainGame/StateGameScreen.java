package com.redigames.teslaminigame.screens.mainGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.redigames.teslaminigame.screens.BaseGameScreen;
import com.redigames.teslaminigame.screens.mainGame.stages.GameStage;
import com.redigames.teslaminigame.screens.mainGame.stages.ScoreStage;

/**
 * Created by Radoslaw Grabowski on 05.07.2017.
 */

public class StateGameScreen extends BaseGameScreen
{
	private GameStage gameStage;
	private ScoreStage scoreStage;

	public StateGameScreen( AssetManager assetManager )
	{
		super( assetManager );

		gameStage = new GameStage( assetManager );
		scoreStage = new ScoreStage( assetManager );
	}

	@Override
	public void loadAssets()
	{
		gameStage.loadAssets();
		scoreStage.loadAssets();
	}

	@Override
	public void show()
	{
		gameStage.show();
		scoreStage.show();
	}

	@Override
	public void render( float delta )
	{
		Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );

		gameStage.draw();
		scoreStage.draw();

		gameStage.act( delta );
		scoreStage.act( delta );
	}

	@Override
	public void onAssetLoadingBegins()
	{
		gameStage.onAssetLoadingBegins();
		scoreStage.onAssetLoadingBegins();
	}

	@Override
	public void renderOnAssetLoading( float deltaTime )
	{
		gameStage.renderOnAssetLoading( deltaTime );
		scoreStage.renderOnAssetLoading( deltaTime );
	}

	@Override
	public void onAssetLoadingEnds()
	{
		gameStage.onAssetLoadingEnds();
		scoreStage.onAssetLoadingEnds();
	}

	@Override
	public void resize( int width, int height )
	{
		gameStage.resize( width, height );
		scoreStage.resize( width, height );
	}

	@Override
	public void pause()
	{
		gameStage.pause();
		gameStage.pause();
	}

	@Override
	public void resume()
	{
		gameStage.resume();
		gameStage.resume();
	}

	@Override
	public void hide()
	{
		gameStage.hide();
		gameStage.hide();
	}

	@Override
	public void dispose()
	{
		gameStage.dispose();
		gameStage.dispose();
	}
}
