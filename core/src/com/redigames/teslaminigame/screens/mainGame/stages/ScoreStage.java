package com.redigames.teslaminigame.screens.mainGame.stages;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.redigames.teslaminigame.actors.ScoreBoard;
import com.redigames.teslaminigame.screens.BaseGameStage;
import com.redigames.teslaminigame.utils.Constants;
import com.redigames.teslaminigame.utils.TextureSizeManipulator;

/**
 * Created by Radoslaw Grabowski on 8/3/2017.
 */

public class ScoreStage extends BaseGameStage
{
	private OrthographicCamera camera;
	private ScoreBoard scoreBoard;

	public ScoreStage( AssetManager assetManager )
	{
		super( assetManager );

		camera = new OrthographicCamera( Constants.GAME_WIDTH, Constants.GAME_HEIGHT );
		camera.position.set( camera.viewportWidth / 2, camera.viewportHeight / 2, 0f );
		camera.update();

		scoreBoard = new ScoreBoard();
	}

	@Override
	public void loadAssets()
	{
		assetManager.load( Constants.GUI_SCORE_BOARD_TEX_PATH, Texture.class );
	}

	@Override
	public void show()
	{

	}

	@Override
	public void resize( int width, int height )
	{

	}

	@Override
	public void onAssetLoadingBegins()
	{

	}

	@Override
	public void renderOnAssetLoading( float deltaTime )
	{

	}

	@Override
	public void onAssetLoadingEnds()
	{
		Texture tex = assetManager.get( Constants.GUI_SCORE_BOARD_TEX_PATH );
		Vector2 size = new Vector2();
		TextureSizeManipulator.FitSizeMaintainRatio( tex, size, true );

		Rectangle pos = new Rectangle( Constants.GAME_WIDTH * 0.95f - size.x, Constants.GAME_HEIGHT * 0.95f - size.y, size.x, size.y );
		scoreBoard.setup( tex, pos );

		addActor( scoreBoard );
	}
}
