package com.redigames.teslaminigame.screens.mainGame.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.redigames.teslaminigame.actors.Background;
import com.redigames.teslaminigame.actors.TeslaHero;
import com.redigames.teslaminigame.logic.ObstaclesManager;
import com.redigames.teslaminigame.logic.TheCurrent;
import com.redigames.teslaminigame.logic.TheGrid;
import com.redigames.teslaminigame.physics.PhysUserData;
import com.redigames.teslaminigame.screens.BaseGameStage;
import com.redigames.teslaminigame.utils.Constants;

import java.util.Random;

public class GameStage extends BaseGameStage implements InputProcessor, ContactListener
{
	private OrthographicCamera camera;

	private Background background1;
	private Background background2;
	private Background background3;
	private Background background4;

	private ObstaclesManager obstaclesManager;
	private TheCurrent current;

	private Rectangle touchArea;

	private Vector3 touchPoint;
	private Texture touchTexture;
	private boolean touchPending;
	private float touchOffset;

	private TeslaHero teslaHero;

	private World physWorld;
	private Box2DDebugRenderer physRenderer;

	private TheGrid theGrid;

	public GameStage( AssetManager assetManager )
	{
		super( assetManager );

		setDebugAll( true );

		// Setup camera
		camera = new OrthographicCamera( Constants.GAME_WIDTH, Constants.GAME_HEIGHT );
		camera.position.set( camera.viewportWidth / 2, camera.viewportHeight / 2, 0f );
		camera.update();

		// physics
		physWorld = new World( Constants.GRAVITY, true );
		physWorld.setContactListener( this );

		if( Constants.PHYS_DEBUG )
		{
			physRenderer = new Box2DDebugRenderer();
			physRenderer.setDrawAABBs( true );
			physRenderer.setDrawBodies( true );
			physRenderer.setDrawInactiveBodies( true );
			physRenderer.setDrawVelocities( true );
		}

		// AC/DC
		current = new TheCurrent();

		// Tesla hero
		teslaHero = new TeslaHero();

		// Obstacle manager
		obstaclesManager = new ObstaclesManager( this, physWorld );

		// Grid
		theGrid = new TheGrid( current, obstaclesManager, Constants.GAME_WIDTH, Constants.GAME_HEIGHT );

		// Set input processor
		Gdx.input.setInputProcessor( this );
	}

	@Override
	public void loadAssets()
	{
		assetManager.load( Constants.BACKGROUND1_TEX_PATH, Texture.class );
		assetManager.load( Constants.BACKGROUND2_TEX_PATH, Texture.class );
		assetManager.load( Constants.BACKGROUND3_TEX_PATH, Texture.class );
		assetManager.load( Constants.BACKGROUND4_TEX_PATH, Texture.class );
		assetManager.load( Constants.TOUCH_BTN_TEX_PATH, Texture.class );
		assetManager.load( Constants.TESLA_TEX_PATH, Texture.class );

		assetManager.load( Constants.COG_TEX_PATH, Texture.class );
		assetManager.load( Constants.GEM_TEX_PATH, Texture.class );
		assetManager.load( Constants.OBSTACLE_TEX_PATH, Texture.class );
	}


	@Override
	public void onAssetLoadingBegins()
	{

	}

	@Override
	public void renderOnAssetLoading( float deltaTime )
	{

	}

	@Override
	public void onAssetLoadingEnds()
	{
		// Touch point setup
		touchPoint = new Vector3( 0, 0, 0 );
		touchArea = new Rectangle( Constants.GAME_WIDTH / 2 - 50, 5, Constants.TOUCH_SIZE.x, Constants.TOUCH_SIZE.y );
		touchPending = false;
		touchOffset = 0f;
		touchTexture = assetManager.get( Constants.TOUCH_BTN_TEX_PATH );

		// Background
		background1 = new Background( ( Texture ) assetManager.get( Constants.BACKGROUND1_TEX_PATH ), 0.2f );
		background4 = new Background( ( Texture ) assetManager.get( Constants.BACKGROUND4_TEX_PATH ), 0.4f );
		background2 = new Background( ( Texture ) assetManager.get( Constants.BACKGROUND2_TEX_PATH ), 0.6f );
		background3 = new Background( ( Texture ) assetManager.get( Constants.BACKGROUND3_TEX_PATH ), 0.9f );
		addActor( background1 );
		addActor( background4 );
		addActor( background2 );
		addActor( background3 );


		// Hero setup
		teslaHero.Setup( ( Texture ) assetManager.get( Constants.TESLA_TEX_PATH ), new Rectangle( 0, Constants.GAME_HEIGHT / 3f, Constants.HERO_SIZE.x, Constants.HERO_SIZE.y ), physWorld, current );
		addActor( teslaHero );

		addActor( current );
		addActor( theGrid );

		obstaclesManager.onAssetLoadingEnds( assetManager );
	}

	////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public void show()
	{

	}

	@Override
	public void resize( int width, int height )
	{
		theGrid.resize( width, height );
		current.resize( width, height );
	}

	public void pause()
	{
	}

	public void resume()
	{
	}

	public void hide()
	{
	}

	////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public void act( float delta )
	{
		super.act( delta );
		physWorld.step( delta, 6, 2 );

		theGrid.update( delta );
		obstaclesManager.update();
	}

	@Override
	public void draw()
	{
		super.draw();

		if( Constants.DRAW_TEXTURES )
		{
			getBatch().begin();
			getBatch().draw( touchTexture, touchArea.x, touchArea.y, touchArea.width, touchArea.height );
			getBatch().end();
		}

		if( Constants.PHYS_DEBUG )
		{
			physRenderer.render( physWorld, camera.combined );
		}
	}


	private void translateScreenToWorldCoordinates( int x, int y )
	{
		getCamera().unproject( touchPoint.set( x, y, 0 ) );
	}

	@Override
	public void dispose()
	{
		Gdx.input.setInputProcessor( null );

		background1.dispose();
		background2.dispose();
		background3.dispose();

		obstaclesManager.dispose();
		current.dispose();
		teslaHero.dispose();
		touchTexture.dispose();

		if( Constants.PHYS_DEBUG )
		{
			physRenderer.dispose();
		}

		physWorld.dispose();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Input processing
	@Override
	public boolean touchDown( int screenX, int screenY, int pointer, int button )
	{
		translateScreenToWorldCoordinates( screenX, screenY );
		if( touchArea.contains( touchPoint.x, touchPoint.y ) )
		{
			touchOffset = touchArea.x - touchPoint.x;
			touchPending = true;
		}

		return super.touchDown( screenX, screenY, pointer, button );
	}

	@Override
	public boolean touchUp( int screenX, int screenY, int pointer, int button )
	{
		touchPending = false;
		touchOffset = 0f;

		return super.touchUp( screenX, screenY, pointer, button );
	}

	@Override
	public boolean touchDragged( int screenX, int screenY, int pointer )
	{
		if( touchPending )
		{
			translateScreenToWorldCoordinates( screenX, screenY );
			float targetPos = touchPoint.x + touchOffset;
			if( targetPos < 0 )
				targetPos = 0;
			else if( targetPos + touchArea.width > Constants.GAME_WIDTH )
				targetPos = Constants.GAME_WIDTH - touchArea.width;

			touchArea.setX( targetPos );
			current.SetAmplitude( targetPos + touchArea.width / 2 - Constants.GAME_WIDTH / 2 );
		}

		return super.touchDragged( screenX, screenY, pointer );
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Contact processing
	@Override
	public void beginContact( Contact contact )
	{
		Body a = contact.getFixtureA().getBody();
		Body b = contact.getFixtureB().getBody();

		PhysUserData da = ( PhysUserData ) a.getUserData();
		PhysUserData db = ( PhysUserData ) b.getUserData();

		if( da.collisionType != db.collisionType )
		{
			//Gdx.app.log( "Collision", "CollisionCollision!!" );
		}
	}

	@Override
	public void endContact( Contact contact )
	{
	}

	@Override
	public void preSolve( Contact contact, Manifold oldManifold )
	{
	}

	@Override
	public void postSolve( Contact contact, ContactImpulse impulse )
	{
	}
}