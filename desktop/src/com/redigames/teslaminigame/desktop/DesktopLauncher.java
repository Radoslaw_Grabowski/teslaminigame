package com.redigames.teslaminigame.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.redigames.teslaminigame.TeslaMinigame;
import com.redigames.teslaminigame.utils.Constants;

public class DesktopLauncher
{
	public static void main( String[] arg )
	{
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.height = ( int ) Constants.GAME_HEIGHT;
		config.width = ( int ) Constants.GAME_WIDTH;

		new LwjglApplication( new TeslaMinigame(), config );
	}
}
